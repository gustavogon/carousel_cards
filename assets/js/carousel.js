let swiper = new Swiper(".slide-content", {
    slidesPerView: 3.5,
    spaceBetween: 25,
    // slidesPerGroup: 2,
    loop: true,
    centerSlide: 'true',
    fade: 'true',
    grabCursor: 'true',
    // loopFillGroupWithBlank: true,
    pagination: {
        // el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    breakpoints: {
        0: {
            slidesPerView: 1,
        },
        576: {
            slidesPerView: 1.5,
            // spaceBetween: 20,
        },
        768: {
            slidesPerView: 2,
        },
        992: {
            slidesPerView: 3,
        },
        1200: {
            slidesPerView: 3.5,
        },
    },
});